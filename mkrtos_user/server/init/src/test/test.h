#pragma once

void mm_test(void);
void ulog_test(void);
void factory_test(void);
void app_test(void);
void mpu_test(void);
void printf_test(void);
void ipc_test(void);
void thread_exit_test(void);
void map_test(void);
void ipc_obj_test(void);
void irq_test(void);
void thread_press_test(void);
void kobj_create_press_test(void);
void sleep_tick(int tick);
void pthread_lock_test(void);
int pthread_cond_lock_test(void);
void sharea_mem_test(void);
void ns_test(void);
